import axios from "axios";
import { ADMINS_LIST } from "../types";

function setAdmins(admins) {
	return {
		type: ADMINS_LIST,
		admins
	};
}

export function fetchAdmins() {
	return async (dispatch, getState, api) => {
		const res = await api.get("/admins");
		dispatch(setAdmins(res.data));
	};
}
