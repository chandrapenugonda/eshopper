import { CURRENT_USER } from "../types";

export function fetchCurrentUser() {
	return async (dispatch, getState, api) => {
		const res = await api.get("/current_user");
		dispatch({ type: CURRENT_USER, payload: res.data });
	};
}
