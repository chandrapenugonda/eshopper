import axios from "axios";
import { USERS_LIST } from "../types";

function setUsers(users) {
	return {
		type: USERS_LIST,
		users
	};
}

export function fetchUsers() {
	return async (dispatch, getState, api) => {
		const res = await api.get("http://react-ssr-api.herokuapp.com/users");
		dispatch(setUsers(res.data));
	};
}
