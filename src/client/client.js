import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import { matchRoutes, renderRoutes } from "react-router-config";
import { hydrate } from "react-dom";
import axios from "axios";
import Routes from "./routes";
import thunk from "redux-thunk";
import reducers from "./reducers";

const composeEnhancers =
	typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
		? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
		: compose;

const axiosInstance = axios.create({
	baseURL: "/api"
});

const enhancer = composeEnhancers(
	applyMiddleware(thunk.withExtraArgument(axiosInstance))
);

const store = createStore(reducers, window.INITIAL_STATE, enhancer);

hydrate(
	<Provider store={store}>
		<BrowserRouter>{renderRoutes(Routes)}</BrowserRouter>
	</Provider>,
	document.getElementById("root")
);
