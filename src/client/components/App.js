import React from "react";
import { renderRoutes } from "react-router-config";
import { fetchCurrentUser } from "../actions/currentUser";
import Header from "./Header";

const App = ({ route }) => {
	return (
		<div>
			<Header />
			{renderRoutes(route.routes)}
		</div>
	);
};

export default {
	component: App,
	loadData: ({ dispatch }) => dispatch(fetchCurrentUser())
};
