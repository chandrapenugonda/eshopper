import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

const Header = ({ auth }) => {
	return (
		<nav className="navbar navbar-inverse" style={{ borderRadius: 0 }}>
			<div className="container-fluid">
				<div className="navbar-header">
					<Link className="navbar-brand" to="/">
						E-shopper
					</Link>
				</div>
				<ul className="nav navbar-nav">
					<li>
						<Link to="/users">Users</Link>
					</li>
					{auth && (
						<li>
							<Link to="/admins">Admins</Link>
						</li>
					)}
				</ul>
				<ul className="nav navbar-nav navbar-right">
					{!auth ? (
						<li>
							<a href="/api/auth/google">
								<span className="glyphicon glyphicon-log-in" />{" "}
								Login
							</a>
						</li>
					) : (
						<li>
							<a href="/api/logout">
								<span className="glyphicon glyphicon-log-out" />{" "}
								Logout
							</a>
						</li>
					)}
				</ul>
			</div>
		</nav>
	);
};

export default connect(state => ({ auth: state.auth }))(Header);
