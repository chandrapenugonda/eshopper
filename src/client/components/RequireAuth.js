import React from "react";
import { connect } from "react-redux";

export default ComposedComponent => {
	class RequireAuth extends React.PureComponent {
		render() {
			switch (this.props.auth) {
				case false:
					return <Redirect to="/" />;
				case null:
					return "Loading.....";
				default:
					<ComposedComponent {...this.props} />;
			}
		}
	}
	return connect(state => ({ auth: state.auth }))(RequireAuth);
};
