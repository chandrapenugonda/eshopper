import React, { Fragment } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { fetchAdmins } from "../actions/admins";

function mapStateToProps(state) {
	return { admins: state.admins };
}

export class AdminsList extends React.Component {
	componentDidMount() {
		this.props.fetchAdmins();
	}
	render() {
		return (
			<Fragment>
				<Helmet>
					<title>Admins page</title>
					<meta name="description" content="Admin page" />
				</Helmet>
				<h1>Admins list</h1>
				<ul>
					{this.props.admins.map(admin => (
						<li key={admin.id}>{admin.name}</li>
					))}
				</ul>
			</Fragment>
		);
	}
}

export function loadData(store) {
	return store.dispatch(fetchAdmins());
}

export default {
	component: connect(
		mapStateToProps,
		{ fetchAdmins }
	)(AdminsList),
	loadData
};
