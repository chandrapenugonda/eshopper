import React from "react";
import { Helmet } from "react-helmet";

const Home = props => {
	return (
		<div>
			<Helmet>
				<title>Home page</title>
				<meta name="description" content="Home page" />
			</Helmet>
			Home page
		</div>
	);
};

export default { component: Home };
