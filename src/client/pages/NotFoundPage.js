import React from "react";
import { Helmet } from "react-helmet";

const NotFound = ({ staticContext = {} }) => {
	staticContext.notFound = true;
	return (
		<div>
			<Helmet>
				<title>We are in shop</title>
				<meta name="description" content="we are in shop" />
			</Helmet>
			Not found page
		</div>
	);
};

export default { component: NotFound };
