import React, { Fragment } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { fetchUsers } from "../actions/users";

function mapStateToProps(state) {
	return { users: state.users };
}

export class UserList extends React.Component {
	componentDidMount() {
		this.props.fetchUsers();
	}
	render() {
		return (
			<Fragment>
				<Helmet>
					<title>Users page</title>
					<meta name="description" content="Users page" />
				</Helmet>
				<h1>Users list</h1>
				<ul>
					{this.props.users.map(user => (
						<li key={user.id}>{user.name}</li>
					))}
				</ul>
			</Fragment>
		);
	}
}

export function loadData(store) {
	return store.dispatch(fetchUsers());
}

export default {
	component: connect(
		mapStateToProps,
		{ fetchUsers }
	)(UserList),
	loadData
};
