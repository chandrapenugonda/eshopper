import { ADMINS_LIST } from "../types";

export default (state = [], action) => {
	switch (action.type) {
		case ADMINS_LIST:
			return action.admins;
		default:
			return state;
	}
};
