import { USERS_LIST } from "../types";

export default (state = [], action) => {
	switch (action.type) {
		case USERS_LIST:
			return action.users;
		default:
			return state;
	}
};
