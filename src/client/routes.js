import React from "react";
import { Route } from "react-router-dom";
import App from "./components/App";
import HomePage from "./pages/HomePage";
import UsersPage from "./pages/UsersListPage";
import AdminsPage from "./pages/AdminsListPage";
import NotFoundPage from "./pages/NotFoundPage";

export default [
	{
		...App,
		routes: [
			{
				...HomePage,
				path: "/",
				exact: true
			},
			{
				...AdminsPage,
				path: "/admins"
			},
			{
				...UsersPage,
				path: "/users"
			},
			{
				...NotFoundPage
			}
		]
	}
];
