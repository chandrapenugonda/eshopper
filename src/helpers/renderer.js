import React from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { renderToString } from "react-dom/server";
import { StaticRouter, Route } from "react-router-dom";
import { Helmet } from "react-helmet";
import serialize from "serialize-javascript";
import { renderRoutes } from "react-router-config";
import routes from "../client/routes";

export default (req, store, context) => {
	const content = renderToString(
		<Provider store={store}>
			<StaticRouter context={context} location={req.path}>
				{renderRoutes(routes)}
			</StaticRouter>
		</Provider>
	);
	const helmet = Helmet.renderStatic();
	return `<html ${helmet.htmlAttributes.toString()}>
	<head> 
	 ${helmet.title.toString()}
            ${helmet.meta.toString()}
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"></head><body>
	<div id="root">${content}</div><script>window.INITIAL_STATE=${serialize(
		store.getState()
	)}</script><script src='bundle.js'></script></body></html>`;
};
