import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import axios from "axios";
import reducers from "../client/reducers";

const composeEnhancers =
	typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
		? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
		: compose;

export default req => {
	const axiosInstance = axios.create({
		baseURL: "http://react-ssr-api.herokuapp.com/",
		headers: { cookie: req.get("cookie") || "" }
	});
	const enhancer = composeEnhancers(
		applyMiddleware(thunk.withExtraArgument(axiosInstance))
	);
	const store = createStore(reducers, {}, enhancer);
	return store;
};
