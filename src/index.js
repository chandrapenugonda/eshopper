import express from "express";
import { matchRoutes } from "react-router-config";
import proxy from "express-http-proxy";
import renderer from "./helpers/renderer";
import createStore from "./helpers/store";
import routes from "./client/routes";

const app = express();
app.use(express.static("public"));
app.use(
	"/api",
	proxy("http://react-ssr-api.herokuapp.com/", {
		proxyReqOptDecorator(opts) {
			opts.headers["x-forwarded-host"] = "localhost:3000";
			return opts;
		}
	})
);
app.get("*", (req, res) => {
	const store = createStore(req);
	const promises = matchRoutes(routes, req.path)
		.map(({ route }) => (route.loadData ? route.loadData(store) : null))
		.map(promise => {
			if (promise) {
				return new Promise(resolve =>
					promise.then(resolve).catch(resolve)
				);
			}
		});
	Promise.all(promises).then(() => {
		const context = {};
		const content = renderer(req, store, context);
		if (content.url) {
			res.redirect(302, content.url);
		}
		if (context.notFound) {
			res.status(404);
		}
		return res.send(content);
	});
});

app.listen(3000);
