var path = require("path");
var merge = require("webpack-merge");
var webpackConfig = require("./webpack.config");

var clientConfig = {
	mode: "development",
	target: "web",
	entry: "./src/client/client.js",
	output: {
		path: path.join(__dirname, "public"),
		filename: "bundle.js"
	}
};

module.exports = merge(webpackConfig, clientConfig);
