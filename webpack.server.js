const path = require("path");
var merge = require("webpack-merge");
var webpackConfig = require("./webpack.config");
var nodeExternals = require("webpack-node-externals");

var serverConfig = {
	mode: "development",
	target: "node",
	entry: "./src/index.js",
	output: {
		path: path.join(__dirname, "build"),
		filename: "bundle.js"
	},
	externals: [nodeExternals()]
};

module.exports = merge(webpackConfig, serverConfig);
